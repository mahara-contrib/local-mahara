<?php

/**
 * This class is just a placeholder, required because Moodle 2 won't let us register
 * any mnet functions for subscribing unless we also register at least one for publishing.
 */
class mahara_mnetservice {
    public function donothing() {
    }
}
